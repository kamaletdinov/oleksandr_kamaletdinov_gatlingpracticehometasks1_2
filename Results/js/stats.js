var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "60",
        "ok": "60",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "206",
        "ok": "206",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "973",
        "ok": "973",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "290",
        "ok": "290",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "212",
        "ok": "212",
        "ko": "-"
    },
    "percentiles1": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "percentiles2": {
        "total": "218",
        "ok": "218",
        "ko": "-"
    },
    "percentiles3": {
        "total": "834",
        "ok": "834",
        "ko": "-"
    },
    "percentiles4": {
        "total": "961",
        "ok": "961",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 53,
    "percentage": 88
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 7,
    "percentage": 12
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.87",
        "ok": "0.87",
        "ko": "-"
    }
},
contents: {
"req_open-home-page-ba7f9": {
        type: "REQUEST",
        name: "Open Home Page",
path: "Open Home Page",
pathFormatted: "req_open-home-page-ba7f9",
stats: {
    "name": "Open Home Page",
    "numberOfRequests": {
        "total": "5",
        "ok": "5",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "825",
        "ok": "825",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "973",
        "ok": "973",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "889",
        "ok": "889",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "62",
        "ok": "62",
        "ko": "-"
    },
    "percentiles1": {
        "total": "864",
        "ok": "864",
        "ko": "-"
    },
    "percentiles2": {
        "total": "953",
        "ok": "953",
        "ko": "-"
    },
    "percentiles3": {
        "total": "969",
        "ok": "969",
        "ko": "-"
    },
    "percentiles4": {
        "total": "972",
        "ok": "972",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 5,
    "percentage": 100
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.072",
        "ok": "0.072",
        "ko": "-"
    }
}
    },"req_click-on-start--8ac91": {
        type: "REQUEST",
        name: "Click on Start button",
path: "Click on Start button",
pathFormatted: "req_click-on-start--8ac91",
stats: {
    "name": "Click on Start button",
    "numberOfRequests": {
        "total": "5",
        "ok": "5",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "207",
        "ok": "207",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "210",
        "ok": "210",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "3",
        "ok": "3",
        "ko": "-"
    },
    "percentiles1": {
        "total": "209",
        "ok": "209",
        "ko": "-"
    },
    "percentiles2": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "percentiles3": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "percentiles4": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.072",
        "ok": "0.072",
        "ko": "-"
    }
}
    },"req_open-age-page-7f2a7": {
        type: "REQUEST",
        name: "Open Age page",
path: "Open Age page",
pathFormatted: "req_open-age-page-7f2a7",
stats: {
    "name": "Open Age page",
    "numberOfRequests": {
        "total": "5",
        "ok": "5",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "217",
        "ok": "217",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "216",
        "ok": "216",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "1",
        "ok": "1",
        "ko": "-"
    },
    "percentiles1": {
        "total": "216",
        "ok": "216",
        "ko": "-"
    },
    "percentiles2": {
        "total": "217",
        "ok": "217",
        "ko": "-"
    },
    "percentiles3": {
        "total": "217",
        "ok": "217",
        "ko": "-"
    },
    "percentiles4": {
        "total": "217",
        "ok": "217",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.072",
        "ok": "0.072",
        "ko": "-"
    }
}
    },"req_entering-age-ec5bb": {
        type: "REQUEST",
        name: "Entering age",
path: "Entering age",
pathFormatted: "req_entering-age-ec5bb",
stats: {
    "name": "Entering age",
    "numberOfRequests": {
        "total": "5",
        "ok": "5",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "208",
        "ok": "208",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "828",
        "ok": "828",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "456",
        "ok": "456",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "303",
        "ok": "303",
        "ko": "-"
    },
    "percentiles1": {
        "total": "209",
        "ok": "209",
        "ko": "-"
    },
    "percentiles2": {
        "total": "827",
        "ok": "827",
        "ko": "-"
    },
    "percentiles3": {
        "total": "828",
        "ok": "828",
        "ko": "-"
    },
    "percentiles4": {
        "total": "828",
        "ok": "828",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 3,
    "percentage": 60
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 2,
    "percentage": 40
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.072",
        "ok": "0.072",
        "ko": "-"
    }
}
    },"req_open-the-larges-5cd63": {
        type: "REQUEST",
        name: "Open the largest value page",
path: "Open the largest value page",
pathFormatted: "req_open-the-larges-5cd63",
stats: {
    "name": "Open the largest value page",
    "numberOfRequests": {
        "total": "5",
        "ok": "5",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "216",
        "ok": "216",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "255",
        "ok": "255",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "228",
        "ok": "228",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "14",
        "ok": "14",
        "ko": "-"
    },
    "percentiles1": {
        "total": "223",
        "ok": "223",
        "ko": "-"
    },
    "percentiles2": {
        "total": "228",
        "ok": "228",
        "ko": "-"
    },
    "percentiles3": {
        "total": "250",
        "ok": "250",
        "ko": "-"
    },
    "percentiles4": {
        "total": "254",
        "ok": "254",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.072",
        "ok": "0.072",
        "ko": "-"
    }
}
    },"req_entering-the-la-3da41": {
        type: "REQUEST",
        name: "Entering the largest value",
path: "Entering the largest value",
pathFormatted: "req_entering-the-la-3da41",
stats: {
    "name": "Entering the largest value",
    "numberOfRequests": {
        "total": "5",
        "ok": "5",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "206",
        "ok": "206",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "223",
        "ok": "223",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "7",
        "ok": "7",
        "ko": "-"
    },
    "percentiles1": {
        "total": "211",
        "ok": "211",
        "ko": "-"
    },
    "percentiles2": {
        "total": "222",
        "ok": "222",
        "ko": "-"
    },
    "percentiles3": {
        "total": "223",
        "ok": "223",
        "ko": "-"
    },
    "percentiles4": {
        "total": "223",
        "ok": "223",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.072",
        "ok": "0.072",
        "ko": "-"
    }
}
    },"req_open-step4-clea-522b7": {
        type: "REQUEST",
        name: "Open Step4 ClearPage",
path: "Open Step4 ClearPage",
pathFormatted: "req_open-step4-clea-522b7",
stats: {
    "name": "Open Step4 ClearPage",
    "numberOfRequests": {
        "total": "5",
        "ok": "5",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "211",
        "ok": "211",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "1",
        "ok": "1",
        "ko": "-"
    },
    "percentiles1": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "percentiles2": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "percentiles3": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "percentiles4": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.072",
        "ok": "0.072",
        "ko": "-"
    }
}
    },"req_click-next-butt-a35a4": {
        type: "REQUEST",
        name: "Click NEXT button",
path: "Click NEXT button",
pathFormatted: "req_click-next-butt-a35a4",
stats: {
    "name": "Click NEXT button",
    "numberOfRequests": {
        "total": "5",
        "ok": "5",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "207",
        "ok": "207",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "225",
        "ok": "225",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "6",
        "ok": "6",
        "ko": "-"
    },
    "percentiles1": {
        "total": "211",
        "ok": "211",
        "ko": "-"
    },
    "percentiles2": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "percentiles3": {
        "total": "223",
        "ok": "223",
        "ko": "-"
    },
    "percentiles4": {
        "total": "225",
        "ok": "225",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.072",
        "ok": "0.072",
        "ko": "-"
    }
}
    },"req_open-step5-onet-108bc": {
        type: "REQUEST",
        name: "Open Step5 OneTimeToken page",
path: "Open Step5 OneTimeToken page",
pathFormatted: "req_open-step5-onet-108bc",
stats: {
    "name": "Open Step5 OneTimeToken page",
    "numberOfRequests": {
        "total": "5",
        "ok": "5",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "207",
        "ok": "207",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "215",
        "ok": "215",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "3",
        "ok": "3",
        "ko": "-"
    },
    "percentiles1": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "percentiles2": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "percentiles3": {
        "total": "215",
        "ok": "215",
        "ko": "-"
    },
    "percentiles4": {
        "total": "215",
        "ok": "215",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.072",
        "ok": "0.072",
        "ko": "-"
    }
}
    },"req_get-code-4035f": {
        type: "REQUEST",
        name: "Get code",
path: "Get code",
pathFormatted: "req_get-code-4035f",
stats: {
    "name": "Get code",
    "numberOfRequests": {
        "total": "5",
        "ok": "5",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "206",
        "ok": "206",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "210",
        "ok": "210",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "208",
        "ok": "208",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "2",
        "ok": "2",
        "ko": "-"
    },
    "percentiles1": {
        "total": "207",
        "ok": "207",
        "ko": "-"
    },
    "percentiles2": {
        "total": "210",
        "ok": "210",
        "ko": "-"
    },
    "percentiles3": {
        "total": "210",
        "ok": "210",
        "ko": "-"
    },
    "percentiles4": {
        "total": "210",
        "ok": "210",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.072",
        "ok": "0.072",
        "ko": "-"
    }
}
    },"req_enter-one-time--b68dc": {
        type: "REQUEST",
        name: "Enter One time token",
path: "Enter One time token",
pathFormatted: "req_enter-one-time--b68dc",
stats: {
    "name": "Enter One time token",
    "numberOfRequests": {
        "total": "5",
        "ok": "5",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "207",
        "ok": "207",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "239",
        "ok": "239",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "215",
        "ok": "215",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "12",
        "ok": "12",
        "ko": "-"
    },
    "percentiles1": {
        "total": "210",
        "ok": "210",
        "ko": "-"
    },
    "percentiles2": {
        "total": "211",
        "ok": "211",
        "ko": "-"
    },
    "percentiles3": {
        "total": "233",
        "ok": "233",
        "ko": "-"
    },
    "percentiles4": {
        "total": "238",
        "ok": "238",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.072",
        "ok": "0.072",
        "ko": "-"
    }
}
    },"req_open-done-page-734e3": {
        type: "REQUEST",
        name: "Open Done page",
path: "Open Done page",
pathFormatted: "req_open-done-page-734e3",
stats: {
    "name": "Open Done page",
    "numberOfRequests": {
        "total": "5",
        "ok": "5",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "208",
        "ok": "208",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "210",
        "ok": "210",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "2",
        "ok": "2",
        "ko": "-"
    },
    "percentiles1": {
        "total": "209",
        "ok": "209",
        "ko": "-"
    },
    "percentiles2": {
        "total": "211",
        "ok": "211",
        "ko": "-"
    },
    "percentiles3": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "percentiles4": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 5,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.072",
        "ok": "0.072",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
