package floodIo

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

class FloodSimulation extends Simulation {

	val th_min = 1
	val th_max = 2
	val test_duration = System.getProperty("duration", "60").toInt
	val test_users = System.getProperty("users", "5").toInt

	val httpProtocol = http
		.baseUrl("https://challenge.flood.io")
		.disableFollowRedirect
		.inferHtmlResources(BlackList(""".*\.js""", """.*\.css""", """.*css.*""", """.*\.gif""", """.*\.jpeg""", """.*\.jpg""", """.*\.ico""", """.*\.woff""", """.*\.woff2""", """.*\.(t|o)tf""", """.*\.png""", """.*detectportal\.firefox\.com.*""", """js""", """png""", """css"""), WhiteList())
		.acceptHeader("text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8")
		.acceptEncodingHeader("gzip, deflate")
		.acceptLanguageHeader("en-CA,en-US;q=0.7,en;q=0.3")
		.userAgentHeader("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:97.0) Gecko/20100101 Firefox/97.0")

	val headers_0 = Map(
		"Cache-Control" -> "max-age=0",
		"Sec-Fetch-Dest" -> "document",
		"Sec-Fetch-Mode" -> "navigate",
		"Sec-Fetch-Site" -> "none",
		"Sec-Fetch-User" -> "?1",
		"Upgrade-Insecure-Requests" -> "1")

	val headers_1 = Map(
		"Origin" -> "https://challenge.flood.io",
		"Sec-Fetch-Dest" -> "document",
		"Sec-Fetch-Mode" -> "navigate",
		"Sec-Fetch-Site" -> "same-origin",
		"Sec-Fetch-User" -> "?1",
		"Upgrade-Insecure-Requests" -> "1")

	val headers_2 = Map(
		"Sec-Fetch-Dest" -> "document",
		"Sec-Fetch-Mode" -> "navigate",
		"Sec-Fetch-Site" -> "same-origin",
		"Sec-Fetch-User" -> "?1",
		"Upgrade-Insecure-Requests" -> "1")

	val headers_9 = Map(
		"Accept" -> "*/*",
		"If-None-Match" -> """"02e07b8cd6516cb70427aca38b568129"""",
		"Sec-Fetch-Dest" -> "empty",
		"Sec-Fetch-Mode" -> "cors",
		"Sec-Fetch-Site" -> "same-origin",
		"X-Requested-With" -> "XMLHttpRequest")



	val scn = scenario("FloodSimulation")
		.exec(http("Open Home Page")
			.get("/")
			.headers(headers_0)
			.check(regex("name=\"authenticity_token\" type=\"hidden\" value=\"(.*?)\"").find.saveAs("authenticity_token"))
			.check(regex(".*step_id.*value=\"(.*?)\"").find.saveAs("step_id")))
		.pause(th_min, th_max)
		.exec(http("Click on Start button")
			.post("/start")
			.headers(headers_1)
			.formParam("utf8", "✓")
			.formParam("authenticity_token", "${authenticity_token}")
			.formParam("challenger[step_id]", "${step_id}")
			.formParam("challenger[step_number]", "1")
			.formParam("commit", "Start")
			.check(status.is(302)))
		.pause(th_min, th_max)
		.exec(http("Open Age page")
			.get("/step/2")
			.headers(headers_2)
				.check(regex(".*step_id.*value=\"(.*?)\"").find.saveAs("step_id"))
				.check(regex("option value=\"(\\d+)\"").findRandom.saveAs("age")))
		.pause(th_min, th_max)
		.exec(http("Entering age")
			.post("/start")
			.headers(headers_1)
			.formParam("utf8", "✓")
			.formParam("authenticity_token", "${authenticity_token}")
			.formParam("challenger[step_id]", "${step_id}")
			.formParam("challenger[step_number]", "2")
			.formParam("challenger[age]", "${age}")
			.formParam("commit", "Next")
			.check(status.is(302)))
		.pause(th_min, th_max)
		.exec(http("Open the largest value page")
			.get("/step/3")
			.headers(headers_2)
			.check(regex(".*step_id.*value=\"(.*?)\"").find.saveAs("step_id"))
			.check(css("label[class='collection_radio_buttons']").findAll.transform(list => list.map(_.toInt).max).saveAs("largest_value"))
			.check(regex("value=\"(.*)\" /><label.*>${largest_value}</label>").saveAs("order_Selected")))
		.pause(th_min, th_max)
		.exec(http("Entering the largest value")
			.post("/start")
			.headers(headers_1)
			.formParam("utf8", "✓")
			.formParam("authenticity_token", "${authenticity_token}")
			.formParam("challenger[step_id]", "${step_id}")
			.formParam("challenger[step_number]", "3")
			.formParam("challenger[largest_order]", "${largest_value}")
			.formParam("challenger[order_selected]", "${order_Selected}")
			.formParam("commit", "Next")
			.check(status.is(302)))
		.pause(th_min, th_max)
		.exec(http("Open Step4 ClearPage")
			.get("/step/4")
			.headers(headers_2)
			.check(regex(".*step_id.*value=\"(.*?)\"").find.saveAs("step_id")))
		.pause(th_min, th_max)
		.exec(http("Click NEXT button")
			.post("/start")
			.headers(headers_1)
			.formParam("utf8", "✓")
			.formParam("authenticity_token", "${authenticity_token}")
			.formParam("challenger[step_id]", "${step_id}")
			.formParam("challenger[step_number]", "4")
			.formParam("challenger[order_3]", "1645095370")
			.formParam("challenger[order_9]", "1645095370")
			.formParam("challenger[order_11]", "1645095370")
			.formParam("challenger[order_11]", "1645095370")
			.formParam("challenger[order_11]", "1645095370")
			.formParam("challenger[order_9]", "1645095370")
			.formParam("challenger[order_11]", "1645095370")
			.formParam("challenger[order_7]", "1645095370")
			.formParam("challenger[order_14]", "1645095370")
			.formParam("challenger[order_10]", "1645095370")
			.formParam("commit", "Next")
			.check(status.is(302)))
		.pause(th_min, th_max)
		.exec(http("Open Step5 OneTimeToken page")
			.get("/step/5")
			.headers(headers_2)
			.check(regex(".*step_id.*value=\"(.*?)\"").find.saveAs("step_id")))
		.pause(th_min, th_max)
		.exec(http("Get code")
			.get("/code")
			.headers(headers_9)
			.check(jsonPath("$.code").saveAs("OneTimeToken")))
		.pause(th_min, th_max)
		.exec(http("Enter One time token")
			.post("/start")
			.headers(headers_1)
			.formParam("utf8", "✓")
			.formParam("authenticity_token", "${authenticity_token}")
			.formParam("challenger[step_id]", "${step_id}")
			.formParam("challenger[step_number]", "5")
			.formParam("challenger[one_time_token]", "${OneTimeToken}")
			.formParam("commit", "Next")
			.check(status.is(302)))
		.pause(th_min, th_max)
		.exec(http("Open Done page")
			.get("/done")
			.headers(headers_2))

		setUp(scn.inject(
			rampUsers(test_users).during(test_duration)
		)).protocols(httpProtocol)
}